# Test de continuous integration (CI) y continuous delivery (CD) con GitLab

## Descripción

Proyecto para emprender en en uso de pruebas continuas y/o de entrega continua 
usando los servicios de gitlab

**Prueba hecha para un hola mundo con `python 2.7`**

```python
    print "Ejemplo de CI con Gitlab y un archivo .py con python 2.7"
    print "========================================================"
    
    print "se deberia generar un hola.txt con el mensaje de 'HOLA MUNDO'"
    
    print "Generando archivo..."
    print "--------------------"
    
    print "  *genenrando archvio"
    archivo = open("hola.txt","w")
    
    print "  *escribiendo archvio"
    archivo.write("HOLA MUNDO")
    
    print "  *guardando archivo"
    archivo.close()
    
    print "Archivo Generado"

```

## Código de .gitlab-ci.yml


```yaml
#image: ubuntu:latest
image: 7ouis/pythontester:latest

stages:
  - build
  - test
  - deploy

compilar:
  stage: build
  script:
  #- apt-get update -qy
  #- apt-get install -y python2.7 python-pip
  #- type python2
  #- type pip2
  - python holamundo.py compilar
  artifacts:
    paths:
    - hola.txt
  
comprobandoResultados:
  stage: test
  script:
  - cat hola.txt
  dependencies:
  - compilar


```

# Información de la imagen Docker para hacer CI
Imagen: `7ouis/pythontester:latest`
* [Proyecto en GitLab de la imagen](https://gitlab.com/LouisP79/PythonTester)
* [Repositorio de la imagen](https://hub.docker.com/r/7ouis/pythontester/)


## Estado

> En Desarrollo :keyboard: :computer: